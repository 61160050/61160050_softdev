import java.util.Scanner;

public class OXfunc {
	static char board [][] = new char [3][3];
	
	public static void printBoard() {
		for(char[] row : board) {
	            for(char col : row) {
			System.out.print(col+" ");
	            }
	            System.out.println();
		}
	 }
	
	public static void setBoard() {
		for(int i=0;i<board.length;i++){
	           for(int j=0;j<board.length;j++){
	              board[i][j]='-'; 
	           }
	        }
		printBoard();
	}
	
	public static void showStart(){
        System.out.println("Welcome to OX Game");
    }
	
	public static void showTurn() {
		boolean stop = true;
    	int turn = 0;
        while (stop) {
            turn++;
            if (turn % 2 == 1) {
                System.out.println("Turn o");
                System.out.print("Please input row,col:");
                getInput('O');
            } else {
                System.out.println("Turn x");
                System.out.print("Please input row,col;");
                getInput('X');
 
            }
        }
	}
	
	public  static  void getInput(char input ){
	       Scanner sc =new Scanner(System.in); 
	       int row = sc.nextInt();
	       int col = sc.nextInt();
	       board[row-1][col-1]= input;
	       printBoard();
	    }
	
	public static void checkInput(int row,int col) {
		
	}
	public static void main(String[] args) {
		showStart();
		setBoard();
		showTurn();
		
	}

}
