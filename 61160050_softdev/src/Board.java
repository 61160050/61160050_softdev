/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LENOVO
 */
public class Board {
    char[][] board ={{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    Player first;
    Player second;
    private Player currentPlayer;
    private Player winner;
    int lastCol;
    int lastRow;
    int countTurn=0;

    public Board(Player first, Player second) {
        this.first = first;
        this.second = second;
        this.currentPlayer=first;
    }

    public char[][] getBoard() {
        return board;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void switchPlayer(){
        if(this.currentPlayer==first){
            this.currentPlayer=second;
        }else{
            this.currentPlayer=first;
        }
    }

    public boolean setRowCol(int row, int col) {
        if(this.board[row-1][col-1]!='-'){
            return false;
        }
       this.board[row-1][col-1]=currentPlayer.getName();
       lastCol = col-1;
       lastRow = row-1;
       countTurn++;
       return true;
    }
    
    public boolean checkCrosswiseL(){
        for(int i=0;i<this.board.length;i++){
            if(this.board[i][i]!=currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
    public boolean checkCrosswiseR(){
       for(int i=0;i<this.board.length;i++){
            if(this.board[i][this.board.length-i-1]!=currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
    public boolean checkRow(){
       for(int col=0;col<this.board[lastRow].length;col++){
           if(this.board[lastRow][col]!=currentPlayer.getName()){
                return false;
           }
       }
       return true;
    }
   
    public boolean checkCol(){
       for(int row=0;row<this.board.length;row++){
           if(this.board[row][lastCol]!=currentPlayer.getName()){
                return false;
           }
       }
       return true;
    }
    
    public  void updateStat(){
        if(this.first==this.winner){
            this.first.win();
            this.second.lose();
        }else if(this.second == this.winner){
            this.second.win();
            this.first.lose();
        }else{
            this.first.draw();
            this.second.draw();
        }
        
    }
    
    public boolean checkWin() {
        if(checkCol()||checkRow()||checkCrosswiseL()||checkCrosswiseR()){
            this.winner=currentPlayer;
            updateStat();
            return true;
        }
        if(countTurn==9){
            return true;
        }
        return false;
    }

    public Player getWinner() {
        return winner;
    }
    
}
