
import java.util.InputMismatchException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author LENOVO
 */
public class Game extends Main {
    private Scanner sc = new Scanner(System.in);
    private int col;
    private int row;
    Board board = null;
    Player O = null;
    Player X = null;
   
    public Game() {
        this.O=new Player('O');
        this.X=new Player('X');
    }
    public  void run(){
        while(true){
            this.runGame();
            if(askContinue()){
                return;
            }
        }
    }
    
    private int getRandomNumber(int min,int max){
        return (int)((Math.random()*(max-min))+min);
    }
    
    public void newGame(){
        if(getRandomNumber(1, 100)%2==0){
            this.board=new Board(O, X);
        }else{
            this.board=new Board(X, O);
        }
    }
    
    public void runGame(){
        this.showWelcome();
        this.newGame();
        while(true){
            this.showBoard();
            this.showTurn();
            inputRowCol();
            if(board.checkWin()){
                showResult();
                showStat();
                return;
            }
            board.switchPlayer();
        }
 
    }

    private void showResult() {
        if(board.getWinner()!=null){
            this.showBoard();
            showWin();
        }else{
            this.showBoard();
            showDraw();
        }
    }

    private void showDraw() {
        System.out.println("Drew!!!");
    }

    private void showWin() {
        System.out.println(board.getWinner().getName()+" Win!!!");
    }
    
    private void showWelcome(){
        System.out.println("Welcome to OX Game");
    }
    
    private void showBoard(){
        char[][] table = this.board.getBoard();
        for(int row=0;row<table.length;row++){
            System.out.print("|");
            for(int col=0;col<table.length;col++){
                System.out.print(" "+table[row][col]+" |");
            }
            System.out.println("");
        }
    }
    
    private void showTurn(){
        System.out.println("Turn "+board.getCurrentPlayer().getName());
    }
    
    private void inputOX(){
        while(true){
            try{
                System.out.print("Please input row col:");
                row = sc.nextInt();
                col = sc.nextInt();
                return;
            }catch(InputMismatchException e){
                System.out.println("Please inpur number 1-3");
                sc.next();
            }
        }
    }

    private void inputRowCol() {
        while(true){
            this.inputOX();
            try{
               if(board.setRowCol(row,col)){
                return;
            } 
            }catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Please input number 1-3");
            }
        }
    }
    private boolean askContinue() {
        while(true){
             System.out.print("Continue y/n?:");
            String ans =sc.next();
            if(ans.equals("n")){
                return true;
            }
            if(ans.equals("y")){
                return false;
            }
        }
    }

    private void showStat() {
        System.out.println(O.getName()+"(win,lose,draw):"+O.getWin()+","+O.getLose()+","+O.getDraw());
        System.out.println(X.getName()+"(win,lose,draw):"+X.getWin()+","+X.getLose()+","+X.getDraw());
    }   
}
