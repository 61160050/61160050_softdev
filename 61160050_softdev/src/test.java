import java.util.Scanner;

public class test {
	
	static char board[][] = {{'-','-','-'},
							 {'-','-','-'},
							 {'-','-','-'}};
	static int turn = 0;
    static int inputRow;
    static int inputCol;
    static int count = 0;
    static int checkError = 0;
    static int checkWin = 0;
    static int checkEnd = 0;
    static int checkSenario = 0;
	public static void showWelcome() {
		System.out.println("Welcome to OX Game");
	}
	
	public static void showBoard() {
		for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + "  ");
            }
            System.out.println();
        }
	}
	
	
	public static void showTurn() {
        if (turn == 0) {
            System.out.println("turn O");
            System.out.print("Please input Row, Col : ");
            inputPosition();
        } else if (turn == 1) {
            System.out.println("turn X");
            System.out.print("Please input Row, Col : ");
            inputPosition();
        }
    }
	
	 public static void inputPosition() {
		 	Scanner sc =new Scanner(System.in);
	        inputRow = sc.nextInt();
	        inputCol = sc.nextInt();
	        checkError();
	        inputBoard();
	 }
	 
	 public static void inputBoard() {
	        if (turn == 0) {
	        	board[inputRow - 1][inputCol - 1] = 'O';
	        	switchTurn();
	        	count++;
	        } else if (turn == 1) {
	        	board[inputRow - 1][inputCol - 1] = 'X';
	        	switchTurn();
	        	count++;
	        }
	  }
 	
	 public static void switchTurn() {
	        if (turn == 0) {
	            checkWin();
	            turn = 1;
	        } else if (turn == 1) {
	            checkWin();
	            turn = 0;
	        }
	  }
	 public static void checkWin() {
	        checkRowO();
	        checkColO();
	        checkRowX();
	        checkColX();
	        checkCrosswiseO();
	        checkCrosswiseX();
	        if (count == 8) {
	        	checkEnd= 3;
	        }
	    }
	 
	 public static void checkRowO() {
	        if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O') {
	        	checkEnd= 1;checkSenario = 1;
	        } else if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O') {
	        	checkEnd= 1;checkSenario = 1;
	        } else if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O') {
	        	checkEnd= 1;checkSenario = 1;
	        } 
	    }
	 
	 public static void checkColO() {
	        if (board[0][0] == 'O' && board[1][0] == 'O'  && board[2][0] == 'O') {
	            checkEnd = 1;checkSenario = 2;
	        } else if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O') {
	            checkEnd = 1;checkSenario = 2;
	        } else if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O' ) {
	            checkEnd = 1;checkSenario = 2;
	        }
	    }
	      
     public static void checkCrosswiseO() {
         if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O') {
             checkEnd = 1;checkSenario = 3;
         } else if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') {
             checkEnd = 1;checkSenario = 3;
         }
     }
     public static void checkRowX() {
         if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') {
             checkEnd = 2;checkSenario = 1;
         } else if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X' ) {
             checkEnd = 2;checkSenario = 1;
         } else if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X' ) {
             checkEnd = 2;checkSenario = 1;
         }
     }

     public static void checkColX() {
         if (board[0][0] == 'X' && board[1][0] == 'X' && board[2][0] == 'X') {
             checkEnd = 2;checkSenario = 2;
         } else if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X') {
             checkEnd = 2;checkSenario = 2;
         } else if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X') {
             checkEnd = 2;checkSenario = 2;
         }
     }

     public static void checkCrosswiseX() {
         if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X') {
             checkEnd = 2;checkSenario = 3;
         } else if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') {
             checkEnd = 2;checkSenario = 3;
         }
     }
     
     public static void showWin() {
         if (checkEnd == 1) {
        	 showBoard();
             System.out.println("Playre O Win !!!");
             showSenario();
             showBye();
         } else if (checkEnd == 2) {
        	 showBoard();
             System.out.println("Playre X Win !!!");
             showSenario();
             showBye();
         } else if (checkEnd  == 3) {
             showBoard();
             System.out.println("Draw !!!");
             System.out.println("Scenario There are no areas on the board. And no one wins");
             showBye();
         }
     }
     
     public static void showSenario() {
    	 if(checkSenario == 1) {
    		 System.out.println("Scenario win in  row");
    	 }else if(checkSenario == 2) {
    		 System.out.println("Scenario win in  Column");
    	 }else if(checkSenario == 3) {
    		 System.out.println("Scenario win in Crosswise");
    	 }
     }
     public static void checkError() {
         if (inputRow >= 3 || inputRow <= 3) {
         } else if (inputCol >= 3 || inputCol <= 3) {
             } else if (board[inputRow][inputCol] == 'O'
                     || board[inputRow][inputCol] == 'X') {
             } else {
                   checkError = 1;
         }
      }
      
     public static void showBye() {
    	 System.out.println("Bye bye...");
     }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        while (checkEnd == 0) {
            showWelcome();
            showBoard();
            showTurn();
            showWin();
        }

	}

}