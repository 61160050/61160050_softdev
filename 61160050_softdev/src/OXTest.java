import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OXTest {

	@Test
	void checkWinCrosswiseL() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.setRowCol(1, 1);
		b.setRowCol(2, 2);
		b.setRowCol(3, 3);
		b.checkCrosswiseL();
	}
	
	@Test
	void checkWinCrosswiseR() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.setRowCol(1, 3);
		b.setRowCol(2, 2);
		b.setRowCol(3, 1);
		b.checkCrosswiseR();
	}
	
	@Test
	void checkWinCol() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.setRowCol(1, 1);
		b.setRowCol(2, 1);
		b.setRowCol(3, 1);
		b.checkWin();
	}
	
	@Test
	void checkWinRow() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.setRowCol(1, 3);
		b.setRowCol(1, 2);
		b.setRowCol(1, 1);
		b.checkWin();
	}
	
	@Test
	 void switchTurn() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.switchPlayer();
	 }
	@Test
	void getBoard() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.getBoard();
	}
	@Test
	void checkDrew() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.countTurn=9;
		b.checkWin();
	}
	@Test
	void checkPlayerWin() {
		Player o = new Player('O');
		Player x = new Player('X');
		Board b = new Board(o,x);
		b.getWinner();
	}
	@Test
	void getWinPlayer() {
		Player o = new Player('O');
		o.getWin();
	}
	
	@Test
	void getLosePlayer() {
		Player o = new Player('O');
		o.getLose();
	}
}
